import streamlit as st
import boto3
import json
from datetime import datetime

s3 = boto3.resource('s3')
st.title("CEI 463 Project")
st.markdown("<h2 style=' color: blue; font-weight: bold; text-decoration: underline;'>Smart Medical Record Management </h2>", unsafe_allow_html=True)

content_object1 = s3.Object('sourcelimnas', 'patients.json')
file_content1 = content_object1.get()['Body'].read().decode('utf-8')
json_content1 = json.loads(file_content1)

content_object2 = s3.Object('sourcelimnas', 'diagnoses.json')
file_content2 = content_object2.get()['Body'].read().decode('utf-8')
json_content2 = json.loads(file_content2)

content_object3 = s3.Object('sourcelimnas', 'medicines.json')
file_content3 = content_object3.get()['Body'].read().decode('utf-8')
json_content3 = json.loads(file_content3)

v1=0
v2=0
v3=0
v4=0
v5=0
v5=0

arrayPatients = []
arrayDiagnoses = []
arrayMedicines = []

for k in json_content1['Patients']:
	arrayPatients.append(json_content1['Patients'][v1]['Name'] + " " + json_content1['Patients'][v1]['Surname'])
	v1+=1


for u in json_content2['Diagnoses']:
	arrayDiagnoses.append(json_content2['Diagnoses'][v2]['Name'])
	v2+=1


for y in json_content3['Medicines']:
	arrayMedicines.append(json_content3['Medicines'][v3]['Brand'])
	v3+=1


patient = st.selectbox("Select a Patient:",arrayPatients)
st.markdown("<p style=' color: red;'>You selected this option: "+patient+"</p>", unsafe_allow_html=True)
patientCode = json_content1['Patients'][arrayPatients.index(patient)]['Code']


diagnosis = st.selectbox("Select a Diagnosis:",arrayDiagnoses)
st.markdown("<p style=' color: red;'>You selected this option: "+diagnosis+"</p>", unsafe_allow_html=True)
diagnosisCode = json_content2['Diagnoses'][arrayDiagnoses.index(diagnosis)]['Code']
diagnosisName= json_content2['Diagnoses'][arrayDiagnoses.index(diagnosis)]['Name']
medicine = st.selectbox("Select a Medicine:",arrayMedicines)
st.markdown("<p style=' color: red;'>You selected this option: "+medicine+"</p>", unsafe_allow_html=True)
medicineCode = json_content3['Medicines'][arrayMedicines.index(medicine)]['Code']

if st.button('Submit'):
	flag=0
	today = datetime.today().strftime('%d/%m/%Y')
	for h in json_content3['Medicines'][arrayMedicines.index(medicine)]['Contraindications']:
		if(diagnosisCode==h):
			flag+=1
	v5+=1

	if(flag>0):
		st.error('This Medicine cannot be administrated to this patient, because it is incompatible with '+diagnosisName)
	elif(flag<1):
			content_object4 = s3.Object('destinationanastasis', patientCode+'.json')
			file_content4 = content_object4.get()['Body'].read().decode('utf-8')
			json_content4 = json.loads(file_content4)
			data = {"date": today,
			"diagnosis_code": diagnosisCode,
			"medicine_code": medicineCode
			}
			
			temp = json_content4['Diagnoses']
			temp.append(data)
			s3object = s3.Object('destinationanastasis', patientCode+'.json')
			s3object.put(Body=(bytes(json.dumps(json_content4).encode('UTF-8'))))
			st.success('Your diagnosis has been inserted into the patient\'s record succesfully!')
else:
	st.markdown("<p style=' color: #3366CC; font-weight: bold;'>Press the above button to insert the diagnosis into the patient's record... </p>", unsafe_allow_html=True)


st.sidebar.header("Team:")
st.sidebar.markdown("<p style=' color: blue; '>Stavros Katsouris </p>", unsafe_allow_html=True)
st.sidebar.markdown("<p style=' color: blue; '>Pavlos Konstantinou </p>", unsafe_allow_html=True)
st.sidebar.markdown("<p style=' color: blue; '>Marios Limnatitis </p>", unsafe_allow_html=True)